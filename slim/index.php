<?php

require "bootstrap.php";
use Chatter\Models\Message; 
use Chatter\Models\User; 
use Chatter\Models\Car; 


$app = new \Slim\App(['settings' => ['displayErrorDetails' => true]]);


//READ MESSAGE - get the message information from database
$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();
    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});
$app->get('/cars/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $cars = Car::find($_id);
    return $response->withStatus(200)->withJson($cars);
});

$app->get('/cars', function($request, $response,$args){
    $_car = new Car();
    $car = $_car->all();
    $payload = [];
    foreach($car as $cr){
        $payload[$cr->id] = [
            'manufacturer'=>$cr->manufacturer,
            'model'=>$cr->model,
            'created_at'=>$cr->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->put('/cars/{car_id}', function($request, $response, $args){
    $name   = $request->getParsedBodyParam('name','');
    $price  = $request->getParsedBodyParam('price','');    
    $_product = Car::find($args['car_id']);
    $_product ->manufacturer =$name;
    $_product ->model =$price;
    if( $_product->save()){
        $payload = ['car_id'=>$_product->id, "result"=>"The Car was updated successfuly"];
        return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});
/*
$app->post('/cars/search', function($request, $response, $args){    
    $name       = $request->getParsedBodyParam('name',''); 
    $products  = Product::where('name', '=', $name)->get(); 
    $payload=[];
    foreach($products as $prd){
        $payload[$prd->id] = [
            'id'=> $prd->id,
            'name'=> $prd->name,
            'price'=> $prd->price
        ];
    }

    return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

*/



$app->get('/messages/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = Message::find($_id);
    return $response->withStatus(200)->withJson($message);
});
$app->get('/users/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $user = User::find($_id);
    return $response->withStatus(200)->withJson($user);
});

//CREATE MESSAGE - insert message to database
$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('userid','');
   $_message = new Message();
   $_message->body = $message;
   $_message->user_id = $userid;
   $_message->save();
   if($_message->id){
       $payload = ['message_id'=>$_message->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});

//DELETE MESSAGE - delete message from database
$app->delete('/cars/{cars_id}', function($request, $response,$args){
    $message = Car::find($args['cars_id']); //find the message in database
    $message->delete(); //delete the message
    if($message->exists){ //check if the message already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //message delete succesfully
    }
});
$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    //die("message id is " . $_message->id);
    $_message->body = $message;
    if($_message->save()){
        $payload = ['message_id'=>$_message->id,"result"=>"The message has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
         return $response->withStatus(400);
    }
});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    Message::insert($payload);
    return $response->withStatus(200)->withJson($payload);

});

//READ USER - get the user information from database
$app->get('/users', function($request, $response,$args){
    $_user = new User(); //create new message
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'username'=>$usr->username,
            'phone_number'=>$usr->phone_number
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//CREATE USER - insert user to database
$app->post('/users', function($request, $response,$args){
    $user = $request->getParsedBodyParam('username',''); //the user sheuzan
    $phone = $request->getParsedBodyParam('phone',''); //the user sheuzan
    $_user = new User(); //create new user
    $_user->username = $user;
    $_user->phone_number = $phone;
    $_user->save();
    if($_user->id){
        $payload = ['id'=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400);
    }
        

});

//DELETE USER - delete user from database
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']); //find the user in database
    $user->delete(); //delete the user
    if($user->exists){ //check if the user already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //user delete succesfully
    }
});


$app->put('/users/{user_id}', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
    $phone = $request->getParsedBodyParam('phone_number','');
    $_user = User::find($args['user_id']);
    //die("message id is " . $_message->id);
    $_user->username = $username;
    $_user->phone_number = $phone;
    if($_user->save()){
        $payload = ['user_id'=>$_user->id,"result"=>"The user has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
         return $response->withStatus(400);
    }
});
/* delete bulk
$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    User::insert($payload);
    return $response->withStatus(200)->withJson($payload);

});
if ($data = $stmt->fetchAll()) {
    print_r(json_encode($data));
}*/


/*$app->delete('/users/bulk', function($request, $response,$args){ 
    $payload = $request->getParsedBody();
    $count = User::count(); 
    $deleteUs = User::latest()->take($count)->skip(1000)->get();
    try{
    foreach($deleteUs as $deleteMe){
                $ids = $deleteMe->id;
            }
    
    User::destroy($ids);
        }
        catch ( Illuminate\Database\QueryException $e) {
            
            var_dump($e->errorInfo);
            
            }
});*/
$app->post('/auth', function($request, $response,$args){//צריך לקבל שם משתשמש וסיסמא, לבדוק זכאות, לשלוח לקליינט   גייסון וב טוקן מתאים 
    $user=$request->getParsedBodyParam('user','');//קיבלנו את היוזרניים מהקליינט
    $password=$request->getParsedBodyParam('password','');
    //we need to do the DB but not now
    if($user == 'jack' && $password =='1234'){//בדיקת זכאות
        //create jwt and send to client, we need to generate jwt but not now
        $payload = ['token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6ImphY2siLCJhZG1pbiI6dHJ1ZX0.PETyPBssJTN1V8fSwPxkE0Hftg3hGYux4Rx1WcMC5NA'];
        return $response->withStatus(200)->withJson($payload);
    }else{
         $payload = ['token'=>null];//אם אין לו טוקן
        return $response->withStatus(403)->withJson($payload);
         }

    });
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});


$app->run();
